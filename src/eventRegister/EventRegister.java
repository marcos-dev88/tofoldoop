package eventRegister;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Snowman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EventRegister implements Listener {

	@EventHandler
	public void sMan(EntityDamageByEntityEvent sManOp) {

		if (sManOp.getCause() == DamageCause.PROJECTILE) {
			if (sManOp.getDamager() instanceof Snowball) {

				if (sManOp.getDamager().getCustomName().equalsIgnoreCase("TofoldoBall")) {
					sManOp.setDamage(2.5);
				} else if (sManOp.getDamager().getCustomName().equalsIgnoreCase("Olha_a_Pedra")) {
					sManOp.setDamage(4.5);
				}
			}
		}
	}

	@EventHandler
	public void sTrow(ProjectileLaunchEvent launch) {
		
		if (launch.getEntity().getShooter() instanceof Snowman) {
			launch.getEntity().setCustomName("TofoldoBall");
			Snowman tofoldo = (Snowman) launch.getEntity().getShooter();
			if (tofoldo.getName().equalsIgnoreCase("Tofoldo")) {
				launch.getEntity().setCustomName("Olha_a_Pedra");
			}
		}
	}

	@EventHandler
	public void noDieWeather(EntityDamageEvent sManTofoldo) {
		
		if (sManTofoldo.getEntity() instanceof Snowman) {
			Snowman tofoldoChuva = (Snowman) sManTofoldo.getEntity();
			if (tofoldoChuva.getName().equalsIgnoreCase("Tofoldo")) {
				/* Para chamar a causa do dano no console */
				// Main.getPlugin(Main.class).getServer().broadcastMessage("Damage: "+ sManTofoldo.getCause());
				if (sManTofoldo.getCause().equals(DamageCause.MELTING)
						|| sManTofoldo.getCause().equals(DamageCause.DROWNING)) {
					sManTofoldo.setCancelled(true);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void moreHearts(PlayerInteractEntityEvent e) {
		
		if (e.getRightClicked() instanceof Snowman) {
			Snowman tolE = (Snowman) e.getRightClicked();
			if (tolE.getName().equalsIgnoreCase("Tofoldo")) {
				tolE.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 3), true);
				tolE.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 2), true);
				tolE.setHealth(tolE.getMaxHealth());
			}
		}
	}

	@EventHandler
	public void cenouraT(PlayerInteractEntityEvent event) {
		
		SnowGolenInteract smanT = new SnowGolenInteract();
		Player player = event.getPlayer();
		Entity smanTInteracted = event.getRightClicked();
		smanT.interactC(player, smanTInteracted);
	}
}
