package eventRegister;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowman;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SnowGolenInteract {

	public ItemStack carrotT() {

		ItemStack cT = new ItemStack(Material.CARROT_ITEM);

		return cT;
	}

	@SuppressWarnings("deprecation")
	public void interactC(Player player, Entity snowGI) {

		if (snowGI instanceof Snowman) {
			Snowman sman = (Snowman) snowGI;
			if (player.getItemInHand().isSimilar(carrotT())) {
				if (sman.getType().equals(EntityType.SNOWMAN)) {
					if (sman.getName().equalsIgnoreCase("Tofoldo")) {
						((Snowman) sman).addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 2400, 2));
						((Snowman) sman).addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 200, 2));
						((Snowman) sman).addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000, 2));
					}
					player.getInventory().getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
				}
			}
		}
	}
}
