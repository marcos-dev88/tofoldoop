package main;

import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import eventRegister.EventRegister;

public class Main extends JavaPlugin {

	PluginManager pm = (PluginManager) this.getServer().getPluginManager();

	@Override
	public void onLoad() {
		this.getServer().broadcastMessage(ChatColor.WHITE + "=======================");
		this.getServer().broadcastMessage(ChatColor.AQUA + "Tofoldo will be op!");
		this.getServer().broadcastMessage(ChatColor.WHITE + "=======================");
	}

	@Override
	public void onEnable() {

		pm.registerEvents(new EventRegister(), this);
		super.onEnable();
	}

	@Override
	public void onDisable() {

		// HandlerList.unregisterall � utilizado para n�o sobrecarregar, ou seja, ele
		// vai apagar o utltimo processo
		HandlerList.unregisterAll();
		super.onDisable();
	}
}
